#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "driver/spi_common.h"
#include "driver/gpio.h"
#include "esp_event.h"

// This is the address that indicates a broadcast
#define BROADCAST_ADDRESS 0xff

ESP_EVENT_DECLARE_BASE(ESP_RADIO_EVENT);

typedef void *rf95_handle_t;
typedef uint32_t rf95_msg_id_t;

typedef struct {
  spi_host_device_t host;
  gpio_num_t cs_pin;
  gpio_num_t irq_pin;
  gpio_num_t rst_pin;
} rf95_config_t;

typedef struct 
{
  int16_t rssi;
  uint8_t length;
  uint8_t data[0];
} rf95_msg_t;

#define RF95_CONFIG_DEFAULT()                       \
  {                                                 \
    .host = CONFIG_RF95_SPI_HOST,                   \
    .cs_pin = CONFIG_RF95_CS_PIN,                   \
    .irq_pin = CONFIG_RF95_IRQ_PIN,                 \
    .rst_pin = CONFIG_RF95_RST_PIN                  \
  }

esp_err_t rf95_send(rf95_handle_t handle, rf95_msg_id_t message_id, const void *data, uint8_t length);
rf95_handle_t rf95_init(const rf95_config_t *config);
esp_err_t rf95_add_handler(rf95_handle_t handle, rf95_msg_id_t message_id, esp_event_handler_t event_handler);
esp_err_t rf95_remove_handler(rf95_handle_t handle, rf95_msg_id_t message_id, esp_event_handler_t event_handler);

#ifdef __cplusplus
}
#endif