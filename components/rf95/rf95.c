#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "freertos/ringbuf.h"
#include "driver/spi_master.h"
#include "esp_log.h"
#include "rf95.h"
#include "data.h"

ESP_EVENT_DEFINE_BASE(ESP_RADIO_EVENT);

static const char *TAG = "rf95";

typedef enum
{
  ModeInitialising = 0, ///< Transport is initialising. Initial default value until init() is called..
  ModeSleep,            ///< Transport hardware is in low power sleep mode (if supported)
  ModeIdle,             ///< Transport is idle.
  ModeTx,               ///< Transport is in the process of transmitting a message.
  ModeRx,               ///< Transport is in the process of receiving a message.
  ModeCad               ///< Transport is in the process of detecting channel activity (if supported)
} rf95_mode;

typedef struct {
  spi_device_handle_t spi_device;
  gpio_num_t irq_pin; 
  gpio_num_t rst_pin;
  bool using_hf_port;
  rf95_mode mode;
  esp_event_loop_handle_t event_loop_handle;
  TaskHandle_t task_handle;

  // tx
  RingbufHandle_t tx_ring_buffer;
} rf95_obj_t;

typedef struct {
  uint32_t id;
  uint8_t data[0];
} rf95_msg_obj_t;

uint8_t rf95_spi_read(spi_device_handle_t device, const uint8_t addr)
{
  spi_transaction_t t = {
    .flags = SPI_TRANS_USE_RXDATA,
    .addr = addr & ~SPI_WRITE_MASK,
    .length = 8,
    .rxlength = 8
  };
  t.rx_data[0] = 0;
  ESP_ERROR_CHECK(spi_device_polling_transmit(device, &t));

  return t.rx_data[0];
}

void rf95_spi_write(spi_device_handle_t device, const uint8_t addr, const uint8_t data)
{
  spi_transaction_t t = {
    .flags = SPI_TRANS_USE_TXDATA,
    .addr = addr | SPI_WRITE_MASK,
    .length = 8
  };
  t.tx_data[0] = data;
  ESP_ERROR_CHECK(spi_device_polling_transmit(device, &t));
}

void rf95_fifo_read(spi_device_handle_t device, void *data, size_t length)
{
  spi_transaction_t t = {
    .addr = RF95_REG_00_FIFO & ~SPI_WRITE_MASK,
    .length = 8 * length,
    .rxlength = 8 * length,
    .rx_buffer = data
  };
  ESP_ERROR_CHECK(spi_device_polling_transmit(device, &t));
}

void rf95_fifo_write(spi_device_handle_t device, void *data, size_t length)
{
  spi_transaction_t t = {
    .addr = RF95_REG_00_FIFO | SPI_WRITE_MASK,
    .length = 8 * length,
    .tx_buffer = data
  };
  ESP_ERROR_CHECK(spi_device_polling_transmit(device, &t));
}

void rf95_idle(rf95_obj_t *rf95)
{
  if (rf95->mode != ModeIdle)
  {
    ESP_LOGD(TAG, "Change mode to idle");
    rf95_spi_write(rf95->spi_device, RF95_REG_01_OP_MODE, RF95_MODE_STDBY);
    rf95->mode = ModeIdle;
  }
}

void rf95_sleep(rf95_obj_t *rf95)
{
  if (rf95->mode != ModeSleep)
  {
    ESP_LOGD(TAG, "Change mode to sleep");
    rf95_spi_write(rf95->spi_device, RF95_REG_01_OP_MODE, RF95_MODE_SLEEP);
    rf95->mode = ModeSleep;
  }
}

void rf95_rx(rf95_obj_t *rf95)
{
  if (rf95->mode != ModeRx)
  {
    ESP_LOGD(TAG, "Change mode to receive");
    rf95_spi_write(rf95->spi_device, RF95_REG_01_OP_MODE, RF95_MODE_RXCONTINUOUS);
    rf95_spi_write(rf95->spi_device, RF95_REG_40_DIO_MAPPING1, 0x00); // Interrupt on RxDone
    rf95->mode = ModeRx;
  }
}

void rf95_tx(rf95_obj_t *rf95)
{
  if (rf95->mode != ModeTx)
  {
    ESP_LOGD(TAG, "Change mode to transmit");
    rf95_spi_write(rf95->spi_device, RF95_REG_01_OP_MODE, RF95_MODE_TX);
    rf95_spi_write(rf95->spi_device, RF95_REG_40_DIO_MAPPING1, 0x40); // Interrupt on TxDone
    rf95->mode = ModeTx;
  }
}

void rf95_cad(rf95_obj_t *rf95)
{
  if (rf95->mode != ModeCad)
  {
    ESP_LOGD(TAG, "Change mode to channel activity detection");
    rf95_spi_write(rf95->spi_device, RF95_REG_01_OP_MODE, RF95_MODE_CAD);
    rf95_spi_write(rf95->spi_device, RF95_REG_40_DIO_MAPPING1, 0x80); // Interrupt on CadDone
    rf95->mode = ModeCad;
  }
}

void rf95_reset(rf95_handle_t handle)
{
  rf95_obj_t *rf95 = (rf95_obj_t *)handle;
  gpio_set_level(rf95->rst_pin, 0);
  vTaskDelay(pdMS_TO_TICKS(1));
  gpio_set_level(rf95->rst_pin, 1);
  vTaskDelay(pdMS_TO_TICKS(10));
}

esp_err_t rf95_send(rf95_handle_t handle, rf95_msg_id_t message_id, const void *data, uint8_t length)
{
  rf95_obj_t *rf95 = (rf95_obj_t *)handle;
  size_t size = sizeof(rf95_msg_obj_t) + length;
  rf95_msg_obj_t *msg_obj;
  UBaseType_t res =  xRingbufferSendAcquire(rf95->tx_ring_buffer, (void **)&msg_obj, size, portMAX_DELAY);
  if (res != pdTRUE)
  {
    ESP_LOGE(TAG, "Failed to aquire memory for message");
    return ESP_FAIL;
  }
  msg_obj->id = message_id;
  memcpy(msg_obj->data, data, length);

  res = xRingbufferSendComplete(rf95->tx_ring_buffer, msg_obj);
  if (res == pdFALSE) {
    return ESP_FAIL;
  }

  return ESP_OK;
}

void rf95_transmit_msg(rf95_obj_t *rf95)
{
  size_t length;
  uint8_t *data = (uint8_t *)xRingbufferReceive(rf95->tx_ring_buffer, &length, 0);
  if (data != NULL)
  {
    ESP_LOGD(TAG, "Package waiting...transmitting");
    rf95_idle(rf95);
    spi_device_acquire_bus(rf95->spi_device, portMAX_DELAY);
    // Position at the beginning of the FIFO
    rf95_spi_write(rf95->spi_device, RF95_REG_0D_FIFO_ADDR_PTR, 0);
    // The message data
    rf95_fifo_write(rf95->spi_device, data, length);
    rf95_spi_write(rf95->spi_device, RF95_REG_22_PAYLOAD_LENGTH, length);
    spi_device_release_bus(rf95->spi_device);

    rf95_tx(rf95); // Start the transmitter

    vRingbufferReturnItem(rf95->tx_ring_buffer, (void *)data);
  }
  else
  {
    rf95_rx(rf95);
  }  
}

void rf95_receive_msg(rf95_obj_t *rf95)
{
  spi_device_acquire_bus(rf95->spi_device, portMAX_DELAY);
  // Have received a packet
  uint8_t size = rf95_spi_read(rf95->spi_device, RF95_REG_13_RX_NB_BYTES);
  rf95_msg_id_t msg_id;
  uint8_t length = size - sizeof(rf95_msg_id_t);
  rf95_msg_t *msg = (rf95_msg_t *)calloc(1, sizeof(rf95_msg_t) + length);
  msg->length = length;

  // Reset the fifo read ptr to the beginning of the packet
  rf95_spi_write(rf95->spi_device, RF95_REG_0D_FIFO_ADDR_PTR, rf95_spi_read(rf95->spi_device, RF95_REG_10_FIFO_RX_CURRENT_ADDR));
  rf95_fifo_read(rf95->spi_device, &msg_id, sizeof(rf95_msg_id_t));
  rf95_fifo_read(rf95->spi_device, msg->data, msg->length);
  rf95_spi_write(rf95->spi_device, RF95_REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags

  // Remember the last signal to noise ratio, LORA mode
  // Per page 111, SX1276/77/78/79 datasheet
  int8_t snr = (int8_t)rf95_spi_read(rf95->spi_device, RF95_REG_19_PKT_SNR_VALUE) / 4;

  // Remember the RSSI of this packet, LORA mode
  // this is according to the doc, but is it really correct?
  // weakest receiveable signals are reported RSSI at about -66
  msg->rssi = rf95_spi_read(rf95->spi_device, RF95_REG_1A_PKT_RSSI_VALUE);

  spi_device_release_bus(rf95->spi_device);

  // Adjust the RSSI, datasheet page 87
  if (snr < 0)
    msg->rssi = msg->rssi + snr;
  else
    msg->rssi = (int)msg->rssi * 16 / 15;
  if (rf95->using_hf_port)
    msg->rssi -= 157;
  else
    msg->rssi -= 164;
      
  esp_event_post_to(rf95->event_loop_handle, ESP_RADIO_EVENT, msg_id, msg, sizeof(rf95_msg_t) + length, portMAX_DELAY);
  free(msg);
}

static void IRAM_ATTR rf95_isr_handler(rf95_handle_t handle)
{
  rf95_obj_t *rf95 = (rf95_obj_t *)handle;
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  vTaskNotifyGiveFromISR(rf95->task_handle, &xHigherPriorityTaskWoken);

  if (xHigherPriorityTaskWoken == pdTRUE)
  {
    portYIELD_FROM_ISR();
  }
}

static void rf95_receiver_task(rf95_handle_t handle)
{
  rf95_obj_t *rf95 = (rf95_obj_t *)handle;
  for(;;) {
    uint32_t notifyCount = ulTaskNotifyTake(pdTRUE, pdMS_TO_TICKS(1000));

    if (notifyCount > 0) {
      uint8_t irq_flags = rf95_spi_read(rf95->spi_device, RF95_REG_12_IRQ_FLAGS);

      if (rf95->mode == ModeRx)
      {
        uint8_t crc_present = rf95_spi_read(rf95->spi_device, RF95_REG_1C_HOP_CHANNEL);
        if (irq_flags & ((irq_flags & (RF95_RX_TIMEOUT | RF95_PAYLOAD_CRC_ERROR))
            | !(crc_present & RF95_RX_PAYLOAD_CRC_IS_ON))) {
          ESP_LOGD(TAG, "Received bad package");
        }
        else if (irq_flags & RF95_RX_DONE)
        {
          ESP_LOGD(TAG, "Receive package done");
          rf95_receive_msg(rf95);
        }
      }
      else if (rf95->mode == ModeTx && irq_flags & RF95_TX_DONE)
      {
        ESP_LOGD(TAG, "Transmit package done");
        rf95_rx(rf95);   
      }

      rf95_spi_write(rf95->spi_device, RF95_REG_12_IRQ_FLAGS, 0xff);
    }
    else
    {
      rf95_transmit_msg(rf95);
    }
    
    esp_event_loop_run(rf95->event_loop_handle, pdMS_TO_TICKS(1000));
  }
  vTaskDelete(NULL);
}

rf95_handle_t rf95_init(const rf95_config_t *config)
{
  ESP_LOGI(TAG, "init");

  rf95_obj_t *rf95 = calloc(1, sizeof(rf95_obj_t));
  if (!rf95) {
    ESP_LOGE(TAG, "Calloc memory for rf95 failed");
    goto err_rf95;
  }

  spi_device_interface_config_t spi_config = {
    .command_bits = 0,
    .address_bits = 8,
    .dummy_bits = 0,
    .mode = 0,
    .duty_cycle_pos = 0,
    .cs_ena_pretrans = 0,
    .cs_ena_posttrans = 1,
    .clock_speed_hz = SPI_MASTER_FREQ_10M,
    .input_delay_ns = 0,
    .spics_io_num = config->cs_pin,
    .flags = 0,
    .queue_size = 1,
    .pre_cb = NULL,
    .post_cb = NULL
  };

  if (spi_bus_add_device(config->host, &spi_config, &rf95->spi_device) != ESP_OK)
  {
    ESP_LOGE(TAG, "Add spi device to host %d failed", config->host);
    goto err_spi_config;
  }

  if (config->rst_pin >= 0) {
    gpio_set_direction(config->rst_pin, GPIO_MODE_OUTPUT);
    gpio_set_level(config->rst_pin, 0);
  }
  gpio_set_direction(config->irq_pin, GPIO_MODE_INPUT);
  gpio_pullup_en(config->irq_pin);
  gpio_set_intr_type(config->irq_pin, GPIO_INTR_POSEDGE);
  gpio_set_level(config->cs_pin, 1);

  rf95->irq_pin = config->irq_pin;
  rf95->rst_pin = config->rst_pin;

  esp_event_loop_args_t loop_args = {
    .queue_size = RADIO_EVENT_LOOP_QUEUE_SIZE,
    .task_name = NULL
  };
  if (esp_event_loop_create(&loop_args, &rf95->event_loop_handle) != ESP_OK) {
    ESP_LOGE(TAG, "Create event loop failed");
    goto err_eloop;
  }

  rf95->tx_ring_buffer = xRingbufferCreate(CONFIG_RF95_RING_BUFFER_SIZE, RINGBUF_TYPE_NOSPLIT);
  if (rf95->tx_ring_buffer == NULL)
  {
    ESP_LOGE(TAG, "Create tx ringbuffer failed");
    goto err_tx_rbuf;
  }

  BaseType_t err = xTaskCreate(rf95_receiver_task, "rf95_receiver_task", CONFIG_RF95_RECEIVER_TASK_STACK_SIZE, rf95, CONFIG_RF95_RECEIVER_TASK_PRIORITY, &rf95->task_handle);
  if (err != pdTRUE) {
    ESP_LOGE(TAG, "Create radio receiver task failed");
    goto err_task_create;
  }

  rf95_reset((rf95_handle_t)rf95);

  uint8_t version = rf95_spi_read(rf95->spi_device, RF95_REG_42_VERSION);
  if (version != 0x12)
  {
    ESP_LOGE(TAG, "Incorrect chip %02x", version);
    goto err_device;
  }

  spi_device_acquire_bus(rf95->spi_device, portMAX_DELAY);
  rf95_spi_write(rf95->spi_device, RF95_REG_01_OP_MODE, RF95_MODE_SLEEP | RF95_LONG_RANGE_MODE);
  
  if (gpio_install_isr_service(0) != ESP_OK)
  {
    ESP_LOGE(TAG, "Install ISR service failed");
    goto err_isr_service_install;
  }
  
  if (gpio_isr_handler_add(rf95->irq_pin, rf95_isr_handler, (void*) rf95) != ESP_OK)
  {
    ESP_LOGE(TAG, "Add ISR handler failed");
    goto err_isr_handler_add;
  }

  rf95_spi_write(rf95->spi_device, RF95_REG_0E_FIFO_TX_BASE_ADDR, 0);
  rf95_spi_write(rf95->spi_device, RF95_REG_0F_FIFO_RX_BASE_ADDR, 0);

  // Set modem config
  rf95_spi_write(rf95->spi_device, RF95_REG_1D_MODEM_CONFIG1, CONFIG_RF95_SIGNAL_BANDWIDTH | CONFIG_RF95_CODING_RATE);
  rf95_spi_write(rf95->spi_device, RF95_REG_1E_MODEM_CONFIG2, CONFIG_RF95_SPREADING_FACTOR | CONFIG_RF95_PAYLOAD_CRC);
  rf95_spi_write(rf95->spi_device, RF95_REG_26_MODEM_CONFIG3, CONFIG_RF95_LOW_DATA_RATE_OPTIMIZE | RF95_AGC_AUTO_ON);
  // Set preamble length
  rf95_spi_write(rf95->spi_device, RF95_REG_20_PREAMBLE_MSB, CONFIG_RF95_PREAMBLE_LENGTH >> 8);
  rf95_spi_write(rf95->spi_device, RF95_REG_21_PREAMBLE_LSB, CONFIG_RF95_PREAMBLE_LENGTH & 0xff);
  // Set frequency
  rf95_spi_write(rf95->spi_device, RF95_REG_06_FRF_MSB, (RF95_FREQ_CONFIG >> 16) & 0xff);
  rf95_spi_write(rf95->spi_device, RF95_REG_07_FRF_MID, (RF95_FREQ_CONFIG >> 8) & 0xff);
  rf95_spi_write(rf95->spi_device, RF95_REG_08_FRF_LSB, RF95_FREQ_CONFIG & 0xff);
  rf95->using_hf_port = (CONFIG_RF95_FREQ >= 779.0);
  // Set power
  rf95_spi_write(rf95->spi_device, RF95_REG_4D_PA_DAC, RF95_PA_DAC_CONFIG);
  rf95_spi_write(rf95->spi_device, RF95_REG_09_PA_CONFIG, RF95_PA_CONFIG);
  spi_device_release_bus(rf95->spi_device);

  rf95_rx(rf95);

  return rf95;

err_isr_handler_add:
  gpio_isr_handler_remove(rf95->irq_pin);
err_isr_service_install:
  gpio_uninstall_isr_service();
err_device:
  spi_bus_remove_device(rf95->spi_device);
err_task_create:
  vRingbufferDelete(rf95->tx_ring_buffer);
err_tx_rbuf:
  esp_event_loop_delete(rf95->event_loop_handle);
err_eloop:
err_spi_config:
err_rf95:
  free(rf95);
  return NULL;
}

esp_err_t rf95_add_handler(rf95_handle_t handle, rf95_msg_id_t message_id, esp_event_handler_t event_handler)
{
  rf95_obj_t *rf95 = (rf95_obj_t *)handle;
  return esp_event_handler_register_with(rf95->event_loop_handle, ESP_RADIO_EVENT, message_id, event_handler, handle);
}

esp_err_t rf95_remove_handler(rf95_handle_t handle, rf95_msg_id_t message_id, esp_event_handler_t event_handler)
{
  rf95_obj_t *rf95 = (rf95_obj_t *)handle;
  return esp_event_handler_unregister_with(rf95->event_loop_handle, ESP_RADIO_EVENT, message_id, event_handler);
}