#include <string.h>
#include "driver/spi_master.h"
#include "esp_log.h"
#include "esp_err.h"

#include "rf95.h"

#define RF95_MISO 19
#define RF95_MOSI 18
#define RF95_CLK   5

static const char *TAG = "rf95-client";

static const char *reply = "And hello back to you";

static void radio_event_handler(rf95_handle_t handle, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
  rf95_msg_t *msg = (rf95_msg_t *)event_data;
  ESP_LOGI(TAG, "Message:\r\n"
    "\tlength:    %u\r\n"
    "\trssi:      %d\r\n"
    "\t%s",
    msg->length, msg->rssi, msg->data);
  if (rf95_send(handle, 0, reply, strlen(reply)))
  {
    ESP_LOGE(TAG, "Send message failed");
  }
}

void app_main(void)
{
  ESP_LOGI(TAG, "Initializing SPI bus");
  spi_bus_config_t bus_config = {
    .miso_io_num = RF95_MISO,
    .mosi_io_num = RF95_MOSI,
    .sclk_io_num = RF95_CLK,
    .quadwp_io_num = -1,
    .quadhd_io_num = -1,
    .flags = SPICOMMON_BUSFLAG_MASTER
  };
  ESP_ERROR_CHECK(spi_bus_initialize(HSPI_HOST, &bus_config, 1));

  rf95_config_t rf95_config = RF95_CONFIG_DEFAULT();

  rf95_handle_t handle = rf95_init(&rf95_config);
  ESP_LOGI(TAG, "Initialized successfully");

  rf95_add_handler(handle, 0, radio_event_handler);
}

